# Lemonade Stand Game

The Lemonade Stand Game is a classic business simulation where players manage their own lemonade stand. The objective is to make strategic decisions on purchasing supplies and setting prices to maximize profits over a series of days. 

## Features

- **Weather Forecast:** Each day, players receive a weather forecast with a ±2°F variance, influencing customer demand.
- **Supply Management:** Players decide how much lemon, sugar, and cups to purchase. Each cup of lemonade consumes 1 lemon, 0.25 cups of sugar, and 1 cup.
- **Dynamic Pricing:** Prices of ingredients adjust based on previous day's purchases, affecting daily costs.
- **Sales Calculation:** Sales are determined by the temperature and price per cup. Higher temperatures generally lead to higher sales, with price sensitivity adjustments.
- **Profit Calculation:** Daily profit is calculated based on sales revenue minus the cost of ingredients.
- **Game Loop:** The game runs for a set number of days, ending when supplies run out or the player decides to stop.
- **User Interface:** Clear display of cash, ingredient prices, weather forecasts, and daily results.

## How to Play

- Each day, decide how much lemon, sugar, and cups to buy.
- View the weather forecast to estimate sales potential.
- Prices adjust based on previous purchases, so plan strategically.
- Monitor daily profit and cash flow to gauge performance.

## About
This project is based on the classic 1979 game "Lemonade Stand," offering a modern interpretation of the original business simulation. Players manage their own lemonade stand by purchasing supplies, setting prices, and navigating daily challenges influenced by weather conditions. The game aims to recreate the strategic decision-making and entrepreneurial spirit of the original while introducing new elements like dynamic pricing and detailed weather forecasts.


## Attributions

I made this game for my kids, because I used to love playing it when I was young. I hope you enjoy it just as much as we do.
