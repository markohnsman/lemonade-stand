const lemonInput = document.querySelector('#lemons')
const sugarInput = document.querySelector('#sugar')
const cupsInput = document.querySelector('#cups')
const totalField = document.querySelector('#total')
const checkoutField = document.querySelector('#checkout')
const playerCashField = document.querySelector('#player-cash')
const playerCupsField = document.querySelector('.player-cups')
const playerSugarField = document.querySelector('.player-sugar')
const playerLemonField = document.querySelector('.player-lemons')
const cupsCostField = document.querySelector('.cups-cost')
const sugarCostField = document.querySelector('.sugar-cost')
const lemonCostField = document.querySelector('.lemons-cost')

// measurement references
// 6 lemons 
// 1 cup white sugar
// 10 Cups
// makes 1/2 gallon
// 10 servings of lemonade



const gameState = {
  store: {
    cups: {
      cost: 5
    },
    sugar: {
      cost: 2.5
    },
    lemons: {
      cost: 8
    },
  },
  player: {
    cash: 20,
    inventory: {
      cups: 0,
      sugar: 0,
      lemons: 0,
    }
  },
  forecast: {
    predicted: 75,
    actual: 75
  },
  day: 1,
  price: 0.50
}

function updateTotal() {
  const cups = gameState.store.cups.cost * cupsInput.value
  const sugar = gameState.store.sugar.cost * sugarInput.value
  const lemons = gameState.store.lemons.cost * lemonInput.value
  const total = cups + sugar + lemons
  totalField.innerText = total.toFixed(2)
  if (total > gameState.player.cash) {
    totalField.style.color = 'red'
    checkoutField.disabled = true
  }
  else {
    totalField.style.color = 'black'
    checkoutField.disabled = false
  }
}

function drawStore() {
  cupsCostField.innerText = gameState.store.cups.cost.toFixed(2)
  sugarCostField.innerText = gameState.store.sugar.cost.toFixed(2)
  lemonCostField.innerText = gameState.store.lemons.cost.toFixed(2)
}

function drawPlayer() {
  playerCashField.innerText = gameState.player.cash.toFixed(2)
  playerCupsField.innerText = gameState.player.inventory.cups
  playerSugarField.innerText = gameState.player.inventory.sugar
  playerLemonField.innerText = gameState.player.inventory.lemons
}

function checkoutCart(e) {
  e.preventDefault()
  const total = parseFloat(totalField.innerText)
  gameState.player.cash -= total
  gameState.player.inventory.cups += parseInt(cupsInput.value) * 100
  gameState.player.inventory.sugar += parseInt(sugarInput.value) * 10
  gameState.player.inventory.lemons += parseInt(lemonInput.value) * 18
  e.target.reset();
  drawPlayer()
  updateTotal()
}

function generateWeather() {
  const current = gameState.forecast.actual
  const random = Math.floor(Math.random() * 5)
  const newActual = current + random
  gameState.forecast.actual = newActual
  gameState.forecast.predicted = newActual + Math.floor(Math.random() * 2) - 2
}

function drawForecast() {
  const forecast = document.querySelector('#temperature')
  forecast.innerText = gameState.forecast.predicted
}

function calculateSales() {
  const fixedCustomers = 20
  const totalCustomers = fixedCustomers + popularityVariance() + temperatureVariance()
  const sales = totalCustomers * calculateDemand()
  return Math.floor(Math.max(0, sales))
}

function popularityVariance() {
  return Math.floor(Math.random() * (20 * gameState.day))
}

function temperatureVariance() {
  return Math.abs(gameState.forecast.actual - 60)
}

function calculateDemand() {
  const temperatureSensitivity = 0.05; // 5% change in demand per degree difference
  const priceSensitivity = 0.50; // 50% change in demand per dollar difference
  const baselinePrice = 0.50;

  const temperatureDifference = gameState.forecast.actual - 80;
  const temperatureImpact = 1 + (temperatureDifference * temperatureSensitivity);

  // Calculate price impact on demand
  const priceDifference = gameState.price - baselinePrice;
  const priceImpact = 1 - (priceDifference * priceSensitivity);

  // Calculate final demand percentage
  const demandPercentage = temperatureImpact * priceImpact;

  return demandPercentage;
}

function cycleDay() {
  generateWeather()
  drawForecast()
}
